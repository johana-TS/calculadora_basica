const nro_1 = document.querySelector("#numero_1");
const nro_2 = document.querySelector("#numero_2");
const result = document.querySelector("#resultado");

function suma (){
    const n1= Number(nro_1.value);
    const n2= Number(nro_2.value);

     result.value= (n1+n2);
    
}
const btn_S = document.querySelector("#btn_sumar");
btn_S.addEventListener("click", suma);

function resta (){
    const n1= Number(nro_1.value);
    const n2= Number(nro_2.value);

     result.value= (n1-n2);
    
}
 document.querySelector("#btn_restar").addEventListener("click", resta);

function division (){
    const n1= Number(nro_1.value);
    const n2= Number(nro_2.value);

     result.value= (n1/n2);
    
}
 document.querySelector("#btn_dividir").addEventListener("click", division);

function multiplicar (){
    const n1= Number(nro_1.value);
    const n2= Number(nro_2.value);

     result.value= (n1*n2);
    
}
 document.querySelector("#btn_multiplicar").addEventListener("click", multiplicar);

